import React, { FC } from 'react'
import classes from '*.module.css';

type Props = {
    contacts: []
}

const Contacts: FC<Props> = ({ contacts }) => {
    return (
        <div>
            <h1>Contact List</h1>
            {
                contacts.map((contact: { name: React.ReactNode; email: React.ReactNode; company: { catchPhrase: React.ReactNode; }; }) => (
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">{contact.name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{contact.email}</h6>
                            <p className="card-text">{contact.company.catchPhrase}</p>
                        </div>
                    </div>
                ))
            }
        </div>
    )
};

export default Contacts